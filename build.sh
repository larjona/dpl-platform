#!/bin/sh

VERSION="1.02"

# Plain version for debian website:
cat wmlheader.html content.html > platform.wml

# More readable version for mine:
cat style.css > platform.html
sed -e "s/:##}//g" content.html >> platform.html

# Plaintext version:
echo "This plaintext version is generated from platform.html" > README.md
echo "using html2text, git tag $VERSION." >> README.md
html2text -style pretty -width 78 platform.html | tail -n +3 >> README.md
