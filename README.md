This plaintext version is generated from platform.html
using html2text, git tag 1.02.


Jonathan Carter
DPL Platform
2019-03-17


jcc@debian.org
https://jonathancarter.org
https://wiki.debian.org/highvoltage



Executive summary

Hi, my name is Jonathan_Carter, also known as highvoltage with the Debian
account name of jcc, and I'm running for DPL.
Broad goals as DPL:

* My intention is not to persue big, hairy and audacious goals. Instead, I aim
  to improve communication to help enable our community to fix existing
  problems and help make ideas flourish.
* I want to promote polish and help drive better quality all over Debian,
  whether that's in our final products or community processes.
* I want Debian to be an attractive project to participate in. Every Debian
  developer should be outright proud to be associated with the project.
* The above might come across as playing it safe, and not being very
  ambitious, but I think big things can be achieved with baby steps and I hope
  to convey that in my platform.


1. Introduction


1.1. Who I am

I am 37 years old and live in Cape Town, South Africa, where I'm also from.
I work part time for an educational institute where I do sysadmin work and
work on a Debian derivative that the institutional network use in their
centres across Africa. The rest of the time I do mostly Debian related
contract work.
I'm a long-time free software advocate who has worked in commercial, non-
profit, public sector and educational environments over the last 15 years.

1.2 Brief history with Debian

I started using Linux in 1999, and in 2003, I was excited about it and wanted
to teach kids about it in schools. I learned that a local non-profit was
looking into similar initiatives and I started talking to them, volunteering,
doing some contract work and then eventually working there full-time. My
colleague at the time, Thomas Black re-introduced me to Debian. I was
previously unimpressed with Debian because it's installer back then, boot-
floppies, almost never worked for me and when it did everything was ancient.
He introduced me to Debian unstable and I got hooked, he told me that I should
learn packaging and apply for DD status and become 'highvoltage@debian.org'.
When he suggested that it felt like a completely alien concept to me, I had
never even thought of considering being part of something as cool as a Linux
distribution project before, but the idea got firmly planted into my head.
Later that year the first Ubuntu version was released, and I took a detour
from Debian into the Ubuntuverse for a few years.
During DebConf7, I discovered the live video streams and got hooked. I also
read the new maintainer's guide for the first time around then. In 2012 I made
it to my first DebConf in Nicaragua which was a great experience, and in 2016
we successfully hosted a DebConf in Cape Town. In 2017 I became part of the
newly formed DebConf Committee. I've made various different contributions to
DebConf over the years, even graphic stories featuring the DebConf chicken.
I became a Debian Maintainer in 2016, and a Debian Developer in 2017. That
makes me a relatively young DD in terms of how long I've been a member. I now
actively maintain over 60 packages and have recently joined the debian-live
project to help improve the quality of our live images.
I try to summarise my Debian activities on my wiki_page, although it's not
always easy keeping it up to date. That page also links to my monthly free
software activity recaps.

1.3. Confidence in Debian

I like the concept of the "Universal Operating System". To me it means that
Debian is adaptable to different technologies, situations and use cases. I'm
constantly amazed at the great work that Debian Developers and all the Debian
contributors do in all shapes and forms on a daily basis.
When Debian was founded, the average computer user was busy migrating from MS-
DOS to Windows 3.1 and learning how to use a mouse. Windows 95 haven't even
been released yet. Since then, so much has changed. The average internet user
now know what a VPN is. We have common awareness of the dangers of mass
surveillance, and the technology that we originally envisioned as providing
answers to all the world's problems are now often used against our species as
a whole.
Immensely positive changes are also happening. The RISC-V_foundation has been
formed, making great strides in developing a high-end free hardware central
processing unit. Their work has also lead to MIPS_following_suite who have
announced that they will release open versions of the MIPS instruction set. At
the same time, Purism is also working towards releasing a fully_free_phone.
With all the good and bad things on our radar, Debian is more relevant than
ever. The world needs a fully free system with stable releases and security
updates that puts its users first, that's commercially friendly but at the
same time doesn't have any hidden corporate agendas. Debian is unique and
beautiful and important, and we shouldn't allow that message to get lost in
the noise that exists out there.
I want us to focus on our shared passion and vision for Debian instead of the
very few places where we differ. Too often, the small things are blown out of
proportion by a small minority or by the media. We can't let that drown out
the vast positivity and goodness in our project that keeps our contributors
churning out good work on a daily basis.

2. My DPL mission statement


2.1. High level goals


  1. I do not intend to solve every problem in Debian during this term: I
     believe it's more beneficial to pull together as a community and work
     together to focus on our most pressing core issues and iterate over them.
  2. I don't want my role as DPL to be purely administrative: I think it's
     critical that there's someone in the project who has a stoic view of the
     Debian project system-wide, since we often tend to get wrapped up in our
     own problems as individuals inside the project.
  3. DPL as enabler: Some of my ideas can be implemented without being a DPL,
     but being DPL comes with some spotlight and attention and makes it a lot
     easier to drive certain ideas forward.
  4. Make community processes a a first class citizen in Debian: As much as
     our technical processes are.
  5. Make the DPL more approachable: Previous DPLs have been great at
     communicating their work to the project, I aim to make that more of a
     two-way street.
  6. Promote new ideas and experimentation in the project: Track the most
     requested ideas in Debian and advertise these for teams who might be
     willing to take them up.
  7. Foster our community: It's important to gain more contributors, but I
     think it's equally important to make contributing to Debian enjoyable for
     existing developers too. When things aren't going well, it feels futile
     recruiting new developers. If we make Debian a fantastic project to work
     in for all our existing developers, we will naturally attract more
     contributors.
  8. Small details matter: We've become too accustomed to small things that
     don't work. If you're used to them, you're also used to working around
     them. Through the eyes of a newcomers though, the Debian experience can
     often feel like a case of death by a thousand papercuts.


2.2. Execution

Overall, I want to help Debian be better at talking about the actual issues
that affect us more often, without having another daunting thread like some
famous ones that had before. I want to go as far as to say that I care more
about fixing communication and our internal problem solving mechanisms than
the actual problems themselves. I hope to promote ways of working that will be
beneficial to Debian long after I'm not DPL any more.
Below are some of the ideas I am considering to implement in order to achieve
the above. They are not stuck in stone, and I will consult with the community
at large when implementing them. If they are deemed largely unsuitable, I will
not mind backing off on any of these and we can work on alternative methods
for solving those problems.

  1. Promote and use #debian-meeting as a meeting room: Many teams have IRC
     meetings in their own channel. It would bring a lot of visibility if
     Debian teams would instead use a slot in the #debian-meeting IRC channel.
     Not only would it make it easier for interested parties to keep up with
     what's going on in the scroll-back, but it might also help attract new
     members to your team as they become familiar with its problems.
  2. Weekly community meetings: Have a project-wide community meeting on a
     weekly basis with the DPL and helpers (in whatever form they may exist)
     present. This would alternate in time to make it easier for people from
     various time-zones. The idea is to shine a light on the most pressing
     issues that concern the community, and work towards solutions. I'm sure
     some people dread the idea because of trolls, but we can moderate and/or
     limit participants if needed.
  3. Implement a 100 papercuts campaign: Create a project to identify the 100
     most annoying small problems with Debian. These would typically be items
     that can be solved with a day's worth of work. This could be a technical
     bug or a problem in the Debian project itself. The aim would be to fix
     these 100 bugs by the time Bullseye (Debian 10) is released. A new cycle
     can also be started as part of an iterative process that's based on time
     and/or remaining bug count.
  4. Hardware enablement project: Between all the new architectures listed
     above, and new devices like free phones- hardware enablement becomes
     really important. I believe we should have budget available for getting
     hardware to developers who care about enabling Debian on those devices. I
     believe the DPL could also spend some time with hardware vendors to get
     some free (or loan) samples, as well as preferential pricing for all
     Debian Developers.
  5. Debian Woman Meetups:As DPL, I wish to create a project where women in
     any city of the world can organise monthly meetings and Debian will cover
     the cost of the refreshments (similar to how it's done for bug squashing
     parties). We often talk about how serious we are about getting more women
     involved in the project, but if we're serious we have to be willing to
     put our money where our mouths are. These meetings could be as simple as
     just talking about Debian and demo'ing it. As soon as someone is
     interested and start using Debian, they immediately learn skills that
     they can transfer to someone else. We have a huge base of potential
     contributors that we're not targetting enough.
  6. Better financial understanding: In the past, people have asked for better
     understanding of how Debian spends funds, and how much is available. The
     way that Debian's accounting works across trusted organisations can make
     real-time exact numbers really difficult, but I think that basic reports
     on a regular basis that lists all recent spending (sprints, DebConf,
     hardware, etc) along with recent balances will be enough to give a
     sufficient overview of how Debian is managing its funds.
  7. DPL pseudo-bugs: As DPL, I will also endeavour to take some time off. I
     plan to not spend weekends working on DPL matters unless there is
     something really urgent (or exciting) that warrants attention. If any
     part of the DPL role becomes too demanding, I will consider it a bug and
     will file a public bug about it that. I recommend a DPL pseudo-package
     (more_on_those_here) that can be used to file bugs against the DPL role,
     and also be used to make common requests to the DPL (similar to how you
     can file ITP/RFS/O/etc bugs in the wnpp pseudo-package).
  8. Make it clear how processes work, and how to submit feedback: I know it
     sounds redundant to say this, but every community process should be well
     documented in an obvious place, but in addition to that, there should
     also be a clear method to file bugs/objections/improvement to a process,
     just as we do with the Debian Policy Manual for packaging. Projects like
     DebConf could really benefit from this in my opinion.

None of the above is set in stone, and I will work with the community as I
pursue these for feedback. Plans may certainly change, but I thing that
working through the above may put the project on a good footing to
strengthening our community and move on to larger ideas. I'll leave you with
one of my favourite quotes:
âGreat minds discuss ideas. Average minds discuss events. Small minds
discuss people.â
â Eleanor Roosevelt

3. Consequences to existing work

If elected, at least to some extent, I plan on cutting back on existing roles
in Debian to maximise the amount of energy available for the DPL role:

  1. DebConf: I'll step down from the DebConf Committee, which shouldn't at
     all be a big disruption since our biggest recurring task is the bid
     decision for the next DebConf, which is now concluded for this year. I
     plan to remain involved in other DebConf areas. I will also step down
     from bursaries after DC19, but will work on contingency for next year.
  2. Package maintenance: I plan to release any ITP bugs that I have filed and
     won't take on any invasive new packaging work. I will however continue
     maintaining my existing packages.
  3. Package sponsoring: This is an activity I do when I have some spare time,
     I will continue doing so, but probably just less of it.
  4. Goofing around: DPLs are supposed to always be serious, right?


4. Some factors to weigh up.


4.1. Reasons to consider me for DPL


  1. New energy: With the above plans (or similar to those), I believe I can
     inject new energy into the Debian project. I think that Debian Developers
     deserve a project leader that can keep the positive energy flowing and
     make everyone proud to be part of the Debian project.
  2. Sanity: I'm not going to introduce any wild ideas to the project,
     instead, I aim to find ways to keep it grounded and create stable
     platforms that we can use to build on towards bigger and more ambitious
     ideas.
  3. Timing: This year happens to be a good time for me to be a DPL. My
     personal life is relatively uneventful right now, and I don't have a
     family to take care of and I have some flexibility work-wise without
     being overcommitted at all.


4.2. Weaknesses in my campaign


  1. Experience: I don't have experience leading a project as large and
     complex (or even complicated) as Debian. I've also only been a Debian
     Developer for a relatively short period of time. However, I believe that
     being aware of this will at least prevent me from flying to close to the
     sun, my main goal for my term will be to reduce existing friction and
     mitigate or solve existing problems.
  2. Public appearances: I live in a relatively remote part of the world, and
     due to visa requirements and travel time, I won't be able to travel and
     talk about Debian as much as previous DPLs were able to. I might be able
     to mitigate this by staying in an area for a slightly longer time and do
     a series of talks at a time, I can often work remotely which help makes
     this possible.
  3. Temperament: I can be very emotional, and sometimes it bursts out a
     little. Over the years I've learned to channel my emotional energy into
     something more positive. I aim to practicea stoistic outlook as DPL and
     spend some time reading up on diplomacy, if elected.


5. Acknowledgements


* I used Zack's_platform_layout as a base for mine.


6. Changelog

This platform is version controlled in a git_repository.

* 1.00: Initial release for public use.
* 1.01: Improve summary on high-level goals.
* 1.02: Build a styled html, plain wml and plain text version of this page,
  fix photo, no text changes.



